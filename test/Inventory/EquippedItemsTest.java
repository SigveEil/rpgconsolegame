package Inventory;

import Heroes.CharacterSheet;
import Items.Armor.ArmorTypes;
import Items.Armor.Armory;
import Items.Slots;
import Items.Weapons.Smithy;
import Items.Weapons.WeaponTypes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EquippedItemsTest {
    @Test
    void canEquipWeapon_playerLevelTooLow_returnsLevel() {
        //Arrange
        EquippedItems EQ = new EquippedItems();
        CharacterSheet player = CharacterSheet.Hercules;
        Smithy weapon = new Smithy("Common Axe",
                2, Slots.WEAPON, WeaponTypes.AXE, 7, 1.1);
        //Returns info to the backend that the level is too low, handle in ifs
        String expected = "level";
        //Act
        String actual = EQ.canEquipWeapon(player, weapon);
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void canEquipArmor_playerLevelTooLow_returnsLevel() {
        //Arrange
        EquippedItems EQ = new EquippedItems();
        CharacterSheet player = CharacterSheet.Hercules;
        Armory armor = new Armory("Common Plate Body Armor",
                2, Slots.BODY, ArmorTypes.PLATE, 1, 0, 0, 2);
        //Returns info to the backend that the level is too low, handle in ifs
        String expected = "level";
        //Act
        String actual = EQ.canEquipArmor(player, armor);
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void canEquipWeapon_playerJobCantEquipItemType_returnsType() {
        //Arrange
        EquippedItems EQ = new EquippedItems();
        CharacterSheet player = CharacterSheet.Hercules;
        Smithy weapon = new Smithy("Common Bow",
                1, Slots.WEAPON, WeaponTypes.BOW, 12, 0.8);
        //Returns info to the backend that the type is wrong, handle in ifs
        String expected = "type";
        //Act
        String actual = EQ.canEquipWeapon(player, weapon);
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void canEquipArmor_playerJobCantEquipItemType_returnsType() {
        //Arrange
        EquippedItems EQ = new EquippedItems();
        CharacterSheet player = CharacterSheet.Hercules;
        Armory armor = new Armory("Common Cloth Head Armor",
                1, Slots.HEAD, ArmorTypes.CLOTH, 0, 0, 5, 1);
        //Returns info to the backend that the type is wrong, handle in ifs
        String expected = "type";
        //Act
        String actual = EQ.canEquipArmor(player, armor);
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void canEquipWeapon_playerCanEquipWeapon_returnsEquip() {
        //Arrange
        EquippedItems EQ = new EquippedItems();
        CharacterSheet player = CharacterSheet.Hercules;
        Smithy weapon = new Smithy("Common Axe",
                1, Slots.WEAPON, WeaponTypes.AXE, 7, 1.1);
        //Returns info to the backend that it can be equipped
        String expected = "equip";
        //Act
        String actual = EQ.canEquipWeapon(player, weapon);
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void canEquipArmor_playerCanEquipArmor_returnsEquip() {
        //Arrange
        EquippedItems EQ = new EquippedItems();
        CharacterSheet player = CharacterSheet.Hercules;
        Armory armor = new Armory("Common Plate Body Armor",
                1, Slots.BODY, ArmorTypes.PLATE, 1, 0, 0, 2);
        //Returns info to the backend that it can be equipped
        String expected = "equip";
        //Act
        String actual = EQ.canEquipArmor(player, armor);
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void calculateDPS_DPSWhenNoWeaponIsEquipped_returnsCorrectDPS() {
        //Arrange
        EquippedItems EQ = new EquippedItems();
        CharacterSheet player = CharacterSheet.Hercules;
        double expected = 1*(1+(5./100));
        //Act
        double actual = EQ.calculateDPS(player, null);
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void calculateDPS_DPSWhenThereIsWeaponButNoArmor_returnsCorrectDPS() {
        //Arrange
        EquippedItems EQ = new EquippedItems();
        CharacterSheet player = CharacterSheet.Hercules;
        Smithy weapon = new Smithy("Common Axe",
                1, Slots.WEAPON, WeaponTypes.AXE, 7, 1.1);
        double expected = (7*1.1)*(1+(5./100));
        //Act
        double actual = EQ.calculateDPS(player, weapon);
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void calculateDPS_DPSWhenThereIsWeaponAndArmor_returnsCorrectDPS() {
        //Arrange
        EquippedItems EQ = new EquippedItems();
        CharacterSheet player = CharacterSheet.Hercules;
        Smithy weapon = new Smithy("Common Axe",
                1, Slots.WEAPON, WeaponTypes.AXE, 7, 1.1);
        Armory armor = new Armory("Common Plate Body Armor",
                1, Slots.BODY, ArmorTypes.PLATE, 1, 0, 0, 2);
        //Equips the armor
        EQ.equipArmor(player, armor);
        double expected = (7. * 1.1) * (1 + ((5.+1) / 100));
        //Act
        double actual = EQ.calculateDPS(player, weapon);
        //Assert
        assertEquals(expected, actual);
    }
}