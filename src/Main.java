import Heroes.*;
import Inventory.EquippedItems;
import Items.Armor.Armory;
import Items.Slots;
import Items.Weapons.Smithy;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        menuSystem();
    }

    //Creating a menu system for the user
    public static void menuSystem() {
        boolean mainMenuLoop = true;
        CharacterSheet player = null;
        Smithy weapon = null;
        Armory armor = null;
        Scanner userInput = new Scanner(System.in);
        while (mainMenuLoop) {
            System.out.println("---RPG CHARACTER SELECTION---");
            System.out.println("Select a character for your epic adventure!");
            System.out.println("Hercules, the Warrior: 1");
            System.out.println("Howard, the Rogue: 2");
            System.out.println("Rainers, the Ranger: 3");
            System.out.println("BabaJaga, the Mage: 4");
            System.out.println("Abandon adventure: 0");

            //Makes sure the user inputs a valid input
            boolean chooseClassLoop = true;
            while (chooseClassLoop) {
                try {
                    int input = userInput.nextInt();
                    if (input == 1) {
                        player = CharacterSheet.Hercules;
                        chooseClassLoop = false;
                        mainMenuLoop = false;
                    } else if (input == 2) {
                        player = CharacterSheet.Howard;
                        chooseClassLoop = false;
                        mainMenuLoop = false;
                    } else if (input == 3) {
                        player = CharacterSheet.Rainers;
                        chooseClassLoop = false;
                        mainMenuLoop = false;
                    } else if (input == 4) {
                        player = CharacterSheet.BabaJaga;
                        chooseClassLoop = false;
                        mainMenuLoop = false;
                    } else if (input == 0) {
                        System.out.println("Thousands will die because of your cowardness...");
                        chooseClassLoop = false;
                        mainMenuLoop = false;
                        System.exit(0);
                        break;
                    } else {
                        System.out.println("---Please enter a valid input---");
                    }
                } catch (Exception e) {
                    System.out.println("---Please enter a valid input---");
                    System.out.println();
                    userInput.nextLine();
                }
            } // while
        } // main menu end

        //Display character stats
        boolean loopStats = true;
        while (loopStats) {
            System.out.println("-----------------------------");
            System.out.println("Name: " + player.name);
            System.out.println("Job: " + player.job);
            System.out.println("Level: " + player.level);
            System.out.println("Stats:");
            System.out.println("   Vitality: " + player.vitality);
            System.out.println("   Strength: " + player.strength);
            System.out.println("   Dexterity: " + player.dexterity);
            System.out.println("   Intelligence: " + player.intelligence);
            System.out.println("   DPS: " + String.format("%.2f", EquippedItems.calculateDPS(player, weapon)));
            System.out.println();
            System.out.println("Hunt monsters (level up): 1");
            System.out.println("Inventory: 2");
            System.out.println("Quit: Any other number");

            //Levels up character
            int wannaLevel = userInput.nextInt();
            if (wannaLevel == 1) {
                System.out.println("-----------------------------");
                System.out.println("         LEVEL UP");
                CharacterSheet.LevelUp(player);
                userInput.nextLine();

                //Open inventory
            } else if (wannaLevel == 2) {
                System.out.println("Look at weapons: 1");
                System.out.println("Look at armor: 2");
                System.out.println("Equipped items: 3");
                int equip = userInput.nextInt();
                userInput.nextLine();

                //Equip weapons
                if (equip == 1) {
                    boolean weaponSelection = true;
                    while (weaponSelection) {
                        System.out.println("Look at Common Axe: 1");
                        System.out.println("Look at Common Bow: 2");
                        System.out.println("Look at Common Dagger: 3");
                        System.out.println("Look at Stick: 4");
                        System.out.println("Look at King-slayer: 5");
                        System.out.println("Return: Any other number");
                        int selection = userInput.nextInt();
                        switch (selection) {
                            case 1 -> weapon = Smithy.commonAxe;
                            case 2 -> weapon = Smithy.commonBow;
                            case 3 -> weapon = Smithy.commonDagger;
                            case 4 -> weapon = Smithy.stick;
                            case 5 -> weapon = Smithy.kingslayer;
                            default -> weaponSelection = false;
                        }
                        if (weapon != null) {
                            System.out.println("-----------------------------");
                            System.out.printf("Name: ");
                            System.out.println(weapon.name);
                            System.out.printf("Weapontype: ");
                            System.out.println(weapon.weaponType);
                            System.out.printf("Required level: ");
                            System.out.println(weapon.requiredLevel);
                            System.out.printf("Damage: ");
                            System.out.println(weapon.damage);
                            System.out.printf("Attacks Per Second: ");
                            System.out.println(weapon.attacksPS);
                            System.out.println("-----------------------------");
                            System.out.println();
                            System.out.println("Do you want to equip this weapon?");
                            System.out.println("Yes: 1    Return: 2");
                            int confirm = userInput.nextInt();
                            userInput.nextLine();
                            if (confirm == 1) {
                                String check = EquippedItems.canEquipWeapon(player, weapon);
                                if (check == "equip") {
                                    EquippedItems.equipWeapon(weapon);
                                    System.out.println("Item equipped!");
                                    weapon = (Smithy) EquippedItems.equipment.get(weapon.itemSlot);
                                    weaponSelection = false;
                                    System.out.println();
                                } else if (check == "type") {
                                    System.out.println("-----------------------------");
                                    System.out.println("Your class cannot equip " + weapon.weaponType);
                                    System.out.println("-----------------------------");
                                } else {
                                    System.out.println("-----------------------------");
                                    System.out.println("You are not high enough level to equip this item");
                                    System.out.println("-----------------------------");
                                }
                            } else System.out.println();
                        }
                    }
                    //Equip Armor
                } else if (equip == 2) {
                    boolean armorSelection = true;
                    while (armorSelection) {
                        System.out.println("Look at Common Plate Body Armor: 1");
                        System.out.println("Look at Common Cloth Head Armor: 2");
                        System.out.println("Look at Common Mail Leg Armor: 3");
                        System.out.println("Return: Any other number");
                        int selection = userInput.nextInt();
                        switch (selection) {
                            case 1 -> armor = Armory.commonPlateBody;
                            case 2 -> armor = Armory.commonClothHead;
                            case 3 -> armor = Armory.commonMailLegs;
                            default -> armorSelection = false;
                        }
                        if (armor != null) {
                            System.out.println("-----------------------------");
                            System.out.printf("Name: ");
                            System.out.println(armor.name);
                            System.out.printf("Weapontype: ");
                            System.out.println(armor.armorType);
                            System.out.println("Item slot: ");
                            System.out.println(armor.itemSlot);
                            System.out.printf("Required level: ");
                            System.out.println(armor.requiredLevel);
                            System.out.printf("Strength: ");
                            System.out.println(armor.strength);
                            System.out.printf("Dexterity: ");
                            System.out.println(armor.dexterity);
                            System.out.printf("Intelligence: ");
                            System.out.println(armor.intelligence);
                            System.out.printf("Vitality: ");
                            System.out.println(armor.vitality);
                            System.out.println("-----------------------------");
                            System.out.println();
                            System.out.println("Do you want to equip this armor?");
                            System.out.println("Yes: 1    Return: 2");
                            int confirm = userInput.nextInt();
                            userInput.nextLine();
                            if (confirm == 1) {
                                String check = EquippedItems.canEquipArmor(player, armor);
                                if (check == "equip") {
                                    EquippedItems.equipArmor(player, armor);
                                    System.out.println("Item equipped!");
                                    armor = (Armory) EquippedItems.equipment.get(armor.itemSlot);
                                    armorSelection = false;
                                    System.out.println();
                                } else if (check == "type") {
                                    System.out.println("-----------------------------");
                                    System.out.println("Your class cannot equip " + armor.armorType);
                                    System.out.println("-----------------------------");
                                } else{
                                    System.out.println("-----------------------------");
                                    System.out.println("You are not high enough level to equip this item");
                                    System.out.println("-----------------------------");
                                }
                            } else System.out.println();
                        }
                    }
                } //Show users equipment
                else if (equip == 3) {
                    System.out.println("--------- Equipment ---------");
                    System.out.printf("Head: ");
                    if (EquippedItems.equipment.get(Slots.HEAD) != null) {
                        System.out.println(EquippedItems.equipment.get(Slots.HEAD).name);
                    } else System.out.println("None");
                    System.out.printf("Body: ");
                    if (EquippedItems.equipment.get(Slots.BODY) != null) {
                        System.out.println(EquippedItems.equipment.get(Slots.BODY).name);
                    } else System.out.println("None");
                    System.out.printf("Legs: ");
                    if (EquippedItems.equipment.get(Slots.LEGS) != null) {
                        System.out.println(EquippedItems.equipment.get(Slots.LEGS).name);
                    } else System.out.println("None");
                    System.out.printf("Weapon: ");
                    if (EquippedItems.equipment.get(Slots.WEAPON) != null) {
                        System.out.println(EquippedItems.equipment.get(Slots.WEAPON).name);
                    } else System.out.println("None");
                }
            } else {
                System.out.println("Farewell.");
                loopStats = false;
            }
        }
    }
}
