package Items.Weapons;

import Items.Item;
import Items.Slots;

public class Smithy extends Item {
    public WeaponTypes weaponType;
    public int damage;
    public double attacksPS;

    //Method to create new Weapons
    public Smithy(String name, int requiredLevel, Slots itemSlot, WeaponTypes weaponType, int damage, double attacksPS) {
        super(name, requiredLevel, itemSlot);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attacksPS = attacksPS;
    }

    //Creates weapons to use in the game
    public static Smithy commonAxe = new Smithy("Common Axe", 1, Slots.WEAPON, WeaponTypes.AXE, 7, 1.1);
    public static Smithy kingslayer = new Smithy("King-slayer", 12, Slots.WEAPON, WeaponTypes.SWORD, 72, 1.2);
    public static Smithy commonBow = new Smithy("Common Bow", 1, Slots.WEAPON, WeaponTypes.BOW, 12, 0.8);
    public static Smithy commonDagger = new Smithy("Common Dagger", 1, Slots.WEAPON, WeaponTypes.DAGGER,4, 2.1);
    public static Smithy stick = new Smithy("Tiny stick", 1, Slots.WEAPON, WeaponTypes.WAND, 1, 1.8);
}
