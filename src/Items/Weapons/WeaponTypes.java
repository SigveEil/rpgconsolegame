package Items.Weapons;

//Defines all the different weapon types in the game
public enum WeaponTypes {
    AXE,
    BOW,
    DAGGER,
    HAMMER,
    STAFF,
    SWORD,
    WAND
}
