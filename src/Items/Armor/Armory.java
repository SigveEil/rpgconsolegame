package Items.Armor;

import Items.Item;
import Items.Slots;

public class Armory extends Item {
    public ArmorTypes armorType;
    public int strength;
    public int dexterity;
    public int intelligence;
    public int vitality;

    //Method to create new Armor
    public Armory(String name, int requiredLevel, Slots itemSlot, ArmorTypes armorType,
                  int strength, int dexterity, int intelligence, int vitality) {
        super(name, requiredLevel, itemSlot);
        this.armorType = armorType;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.vitality = vitality;
    }
    //Creates armor to use in the game
    public static Armory commonPlateBody = new Armory("Common Plate Body Armor", 1, Slots.BODY, ArmorTypes.PLATE,
            1, 0, 0, 2);
    public static Armory commonClothHead = new Armory("Common Cloth Head Armor", 1, Slots.HEAD,
            ArmorTypes.CLOTH, 0, 0, 5, 1);
    public static Armory commonMailLegs = new Armory("Common Mail Leg Armor", 1, Slots.LEGS, ArmorTypes.PLATE,
            1, 0, 0, 1);
}
