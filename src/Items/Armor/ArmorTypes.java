package Items.Armor;

//Defines all the different armor types in the game
public enum ArmorTypes {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
