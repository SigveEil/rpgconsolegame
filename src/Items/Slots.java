package Items;

//Defines all the slots items can be equipped to
public enum Slots {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
