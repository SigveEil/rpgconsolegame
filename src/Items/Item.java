package Items;

public abstract class Item {
    public String name;
    public int requiredLevel;
    public Slots itemSlot;

    public Item(String name, int requiredLevel, Slots itemSlot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.itemSlot = itemSlot;
    }
}
