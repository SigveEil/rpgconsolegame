package Heroes;

import Heroes.Jobs.Mage;
import Heroes.Jobs.Ranger;
import Heroes.Jobs.Rogue;
import Heroes.Jobs.Warrior;
import Items.Armor.ArmorTypes;
import Items.Weapons.WeaponTypes;

//Defining everything a character needs
public class CharacterSheet extends Character {
    public int strength;
    public int dexterity;
    public int intelligence;
    public int vitality;
    public int bonusStrength;
    public int bonusDexterity;
    public int bonusIntelligence;
    public int bonusVitality;
    public double primaryAttribute;
    public ArmorTypes[] armorTypes;
    public WeaponTypes[] weaponTypes;

    //Creating characters for the user
    public CharacterSheet(String name, int level, String job, int vitality, int strength, int dexterity, int intelligence,
                          int bonusStrength, int bonusDexterity, int bonusIntelligence, int bonusVitality,
                          double primaryAttribute, ArmorTypes[] armorTypes, WeaponTypes[] weaponTypes) {
        super(name, level, job);
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.bonusVitality = bonusVitality;
        this.bonusDexterity = bonusDexterity;
        this.bonusIntelligence = bonusIntelligence;
        this.bonusStrength = bonusStrength;
        this.primaryAttribute = primaryAttribute;
        this.armorTypes = armorTypes;
        this.weaponTypes = weaponTypes;
    }

    //Creates one character for each job
    public static CharacterSheet Howard = new CharacterSheet("Howard", 1, "Rogue", Rogue.vitality,
            Rogue.strength, Rogue.dexterity, Rogue.intelligence, Rogue.bonusStrength, Rogue.bonusDexterity,
            Rogue.bonusIntelligence, Rogue.bonusVitality, Rogue.primaryAttribute, Rogue.armorTypes, Rogue.weaponTypes);

    public static CharacterSheet BabaJaga = new CharacterSheet("BabaJaga", 1, "Mage", Mage.vitality,
            Mage.strength, Mage.dexterity, Mage.intelligence, Mage.bonusStrength, Mage.bonusDexterity,
            Mage.bonusIntelligence, Mage.bonusVitality, Mage.primaryAttribute, Mage.armorTypes, Mage.weaponTypes);

    public static CharacterSheet Rainers = new CharacterSheet("Rainers", 1, "Ranger", Ranger.vitality,
            Ranger.strength, Ranger.dexterity, Ranger.intelligence, Ranger.bonusStrength, Ranger.bonusDexterity,
            Ranger.bonusIntelligence, Ranger.bonusVitality, Ranger.primaryAttribute, Ranger.armorTypes, Ranger.weaponTypes);

    public static CharacterSheet Hercules = new CharacterSheet("Hercules", 1, "Warrior", Warrior.vitality,
            Warrior.strength, Warrior.dexterity, Warrior.intelligence, Warrior.bonusStrength, Warrior.bonusDexterity,
            Warrior.bonusIntelligence, Warrior.bonusVitality, Warrior.primaryAttribute, Warrior.armorTypes, Warrior.weaponTypes);

    //Method to level up a character
    public static void LevelUp(CharacterSheet player) {
        player.level++;
        player.vitality += player.bonusVitality;
        player.strength += player.bonusStrength;
        player.dexterity += player.bonusDexterity;
        player.intelligence += player.bonusIntelligence;
        String job = player.job;
        int incrPA = switch (job) {
            case "Mage", "Ranger" -> 5;
            case "Rogue" -> 4;
            case "Warrior" -> 3;
        };
        player.primaryAttribute += incrPA;
    }
}
