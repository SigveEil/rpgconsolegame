package Heroes.Jobs;

import Items.Armor.ArmorTypes;
import Items.Weapons.WeaponTypes;

public class Mage {
    //Setting base stats
    public static int vitality = 5;
    public static int strength = 1;
    public static int dexterity = 1;
    public static int intelligence = 8;

    //Setting level-up stats
    public static int bonusVitality = 3;
    public static int bonusStrength = 1;
    public static int bonusDexterity = 1;
    public static int bonusIntelligence = 5;

    //Setting primaryAttribute, armor and weapon types
    public static int primaryAttribute = intelligence;
    public static ArmorTypes[] armorTypes = new ArmorTypes[]{ArmorTypes.CLOTH};
    public static WeaponTypes[] weaponTypes = new WeaponTypes[]{WeaponTypes.STAFF, WeaponTypes.WAND};
}
