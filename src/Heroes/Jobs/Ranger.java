package Heroes.Jobs;

import Items.Armor.ArmorTypes;
import Items.Weapons.WeaponTypes;

public class Ranger {
    //Setting base stats
    public static int vitality = 8;
    public static int strength = 1;
    public static int dexterity = 7;
    public static int intelligence = 1;

    //Setting level-up stats
    public static int bonusVitality = 2;
    public static int bonusStrength = 1;
    public static int bonusDexterity = 5;
    public static int bonusIntelligence = 1;

    //Setting primaryAttribute, armor and weapon types
    public static int primaryAttribute = dexterity;
    public static ArmorTypes[] armorTypes = new ArmorTypes[] {ArmorTypes.MAIL, ArmorTypes.LEATHER};
    public static WeaponTypes[] weaponTypes = new WeaponTypes[] {WeaponTypes.BOW};
}
