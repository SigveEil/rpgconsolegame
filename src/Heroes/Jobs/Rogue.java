package Heroes.Jobs;

import Items.Armor.ArmorTypes;
import Items.Weapons.WeaponTypes;

public class Rogue {
    //setting base stats
    public static int vitality = 8;
    public static int strength = 2;
    public static int dexterity = 6;
    public static int intelligence = 1;

    //setting level-up stats
    public static int bonusVitality = 3;
    public static int bonusStrength = 1;
    public static int bonusDexterity = 4;
    public static int bonusIntelligence = 1;

    //setting primary attribute, armor and weapon types
    public static int primaryAttribute = dexterity;
    public static ArmorTypes[] armorTypes = new ArmorTypes[] {ArmorTypes.MAIL, ArmorTypes.LEATHER};
    public static WeaponTypes[] weaponTypes = new WeaponTypes[] {WeaponTypes.DAGGER, WeaponTypes.SWORD};
}
