package Heroes.Jobs;

import Items.Armor.ArmorTypes;
import Items.Weapons.WeaponTypes;

public class Warrior {
    //Setting base stats
    public static int vitality = 10;
    public static int strength = 5;
    public static int dexterity = 2;
    public static int intelligence = 1;

    //Setting level-up stats
    public static int bonusVitality = 5;
    public static int bonusStrength = 3;
    public static int bonusDexterity = 2;
    public static int bonusIntelligence = 1;

    //Setting primaryAttribute, armor and weapon types
    public static int primaryAttribute = strength;
    public static ArmorTypes[] armorTypes = new ArmorTypes[] {ArmorTypes.MAIL, ArmorTypes.PLATE};
    public static WeaponTypes[] weaponTypes = new WeaponTypes[] {WeaponTypes.AXE, WeaponTypes.HAMMER, WeaponTypes.SWORD};
}
