package Heroes;

public abstract class Character {
    public String name;
    public int level = 1;
    public String job;

    public Character(String name, int level, String job) {
        this.name = name;
        this.level = level;
        this.job = job;
    }
}
