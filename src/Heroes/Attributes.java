package Heroes;

public enum Attributes {
     STRENGTH,
     DEXTERITY,
     INTELLIGENCE,
     VITALITY
}
