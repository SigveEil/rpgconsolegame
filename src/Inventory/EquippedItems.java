package Inventory;

import Heroes.CharacterSheet;
import Items.Armor.ArmorTypes;
import Items.Armor.Armory;
import Items.Item;
import Items.Slots;
import Items.Weapons.Smithy;
import Items.Weapons.WeaponTypes;
import java.util.Arrays;
import java.util.HashMap;

public class EquippedItems {
    //Creates hashmap for equipped items
    public static HashMap<Slots, Item> equipment = new HashMap<Slots, Item>();

    //Checks if the player can equip an item
    public static String canEquipWeapon(CharacterSheet player, Smithy weapon) {
        String equip = "";
        WeaponTypes lookingAt = weapon.weaponType;
        boolean checkTypes = Arrays.asList(player.weaponTypes).contains(lookingAt);
        if(player.level >= weapon.requiredLevel && checkTypes) {
            equip = "equip";
        } else if(checkTypes) {
            equip = "level";
        } else equip = "type";
        return equip;
    }
    //Checks if the player can equip the armor
    public static String canEquipArmor(CharacterSheet player, Armory armor) {
        String equip = "";
        ArmorTypes lookingAt = armor.armorType;
        boolean checkTypes = Arrays.asList(player.armorTypes).contains(lookingAt);
        if(player.level >= armor.requiredLevel && checkTypes) {
            equip = "equip";
        } else if(checkTypes) {
            equip = "level";
        } else equip = "type";
        return equip;
    }

    //Equips weapon
    public static void equipWeapon(Smithy weapon) {
        equipment.put(weapon.itemSlot, weapon);
    }
    //Equips armor
    public static void equipArmor(CharacterSheet player, Armory armor) {
        equipment.put(armor.itemSlot, armor);
        player.strength += armor.strength;
        player.dexterity += armor.dexterity;
        player.intelligence += armor.intelligence;
        player.vitality += armor.vitality;
        String job = player.job;
        //Adds to primary attribute based on the players job
        switch (job) {
            case "Rogue", "Ranger" -> player.primaryAttribute += armor.dexterity;
            case "Mage" -> player.primaryAttribute += armor.intelligence;
            case "Warrior" -> player.primaryAttribute += armor.strength;
        }
    }

    //Calculates DPS
    public static double calculateDPS(CharacterSheet player, Smithy weapon) {
        double weaponDPS = 1.00;
        //Checks if weapon is equipped
        if(weapon != null) {
            weaponDPS = weapon.damage * weapon.attacksPS;
        }
        double DPS = (1 * (1 + (player.primaryAttribute / 100)));
        return DPS = DPS * weaponDPS;
    }
}
