Skeleton for RPG Console game!
Select your character to play:
    4 unique jobs to choose from, pick your playstyle!
Features:
    - Fight monsters to level up your character and watch your stats improve
    - Equip awesome weapons
    - Equip unique armor
    - View your equipped items
    - Some comments
    - More spaghetti than Italy!
Tools:
    - This epic adventure was written using Java
    - Lots of coffee